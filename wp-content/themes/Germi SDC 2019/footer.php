<?php
$social=get_field('social', 'option');
$fb=$social['facebook'];
$tw=$social['twitter'];
$ig=$social['instagram'];


$info_footer=get_field('info_footer', 'option');


?>
            <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <?php if (!empty($fb) || !empty($ig) || !empty($tw)) { ?>
                    <ul class="icons">
                        <?php if (!empty($fb)){ ?>
                            <li><a href="<?php echo $fb;?>" class="icon fa-facebook">
                                    <span class="label">Facebook</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (!empty($tw)){ ?>
                            <li>
                                <a href="<?php echo $tw;?>" class="icon fa-twitter">
                                    <span class="label">Twitter</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (!empty($ig)){ ?>
                            <li><a href="<?php echo $ig;?>" class="icon fa-instagram">
                                    <span class="label">Instagram</span>
                                </a></li>
                        <?php } ?>
                    </ul>
                    <?php }?>
                    <?php if (!empty($info_footer)){ ?>
                    <ul class="copyright">
                        <?php while (have_rows('info_footer', 'option')) { the_row(); ?>
                            <li class="info_footer"><?php echo get_sub_field('info');?></li>
                        <?php } ?>

                    </ul>
                    <?php } ?>
                </div>
            </footer>

            <!-- Scripts -->
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/skel.min.js"></script>
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/util.js"></script>
            <!--[if lte IE 8]><script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie/respond.min.js"></script><![endif]-->
            <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>


<?php wp_footer(); ?>

<?php if (is_singular('gallery')){ ?>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/galleries.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
        });
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/galleries/picturefill.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/galleries/lightgallery-all.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/galleries/jquery.mousewheel.min.js"></script>

<?php } ?>

<?php if (is_post_type_archive('eventi')){ ?>
    <!--<script type="text/javascript"-->
    <!--        src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">-->
    <!--</script>-->

    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-combined.min.css" rel="stylesheet">

    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/assets/js/calendar/bootstrap.min.js">
    </script>
    <script type="text/javascript"
            src="<?php echo get_template_directory_uri(); ?>/assets/js/calendar/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript">
        $('#datetimepicker').datetimepicker({
            format: 'dd/MM/yyyy',
            language: 'it_IT',
            pickTime: false,            // disables de time picker
        });
        $.fn.datetimepicker.defaults = {
            maskInput: true,           // disables the text input mask
            pickDate: true,            // disables the date picker
            pickTime: true,            // disables de time picker
            pick12HourFormat: false,   // enables the 12-hour format time picker
            pickSeconds: true,         // disables seconds in the time picker
            startDate: -Infinity,      // set a minimum date
            endDate: Infinity          // set a maximum date
        };
    </script>
    <script type="text/javascript">
        $('#datetimepicker2').datetimepicker({
            format: 'dd/MM/yyyy',
            language: 'it-IT',
            pickTime: false,            // disables de time picker

        });
        $.fn.datetimepicker.defaults = {
            maskInput: true,           // disables the text input mask
            pickDate: true,            // disables the date picker
            pickTime: true,            // disables de time picker
            pick12HourFormat: false,   // enables the 12-hour format time picker
            pickSeconds: false,         // disables seconds in the time picker
            startDate: -Infinity,      // set a minimum date
            endDate: Infinity          // set a maximum date
        };
    </script>



<?php }?>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c75beabb9b227f0"></script>

            </body>
</html>