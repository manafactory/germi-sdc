<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="<?php echo get_template_directory_uri(); ?>/assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/ie8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/ie9.css" /><![endif]-->


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>

</head>
<?php

$ga=get_field('g_a_codice','option');
if (!empty($ga)){

    ?>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga;?>"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '<?php echo $ga;?>');
    </script><?php }
?>

<body class="landing">

<!-- Header -->
<header id="header"<?php if (is_home()){ ?> class="alt"<?php } ?>>
	<?php if( is_front_page() ) { ?>
    <h1 id="logo"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="GERMI LDC"></a></h1>
	<?php } else { ?>
    <h1 id="logo" class="innerlogo"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo_i.png" alt="GERMI LDC"></a></h1>
	<?php }  ?>
    <a href="#nav">Menu</a>
</header>


<nav id="nav">
<?php

$social=get_field('social', 'option');
$fb=$social['facebook'];
$tw=$social['twitter'];
$ig=$social['instagram'];

$social_for_menu='';

if ((!empty($fb) || !empty($ig) || !empty($tw))){
    $social_for_menu.='<span class="socialspan">Seguici su:</span>';
    $social_for_menu.='<ul class="icons">';
    if (!empty($fb)){
        $social_for_menu.='<li><a href="'. $fb.'" class="icon fa-facebook"><span class="label">Facebook</span></a></li>';
    }
    if (!empty($tw)){
        $social_for_menu.='<li><a href="'.$tw.'" class="icon fa-twitter"><span class="label">Twitter</span></a></li>';
    }
    if (!empty($ig)){
        $social_for_menu.='<li><a href="'.$tw.'" class="icon fa-instagram"><span class="label">Instagram</span></a></li>';
    }
    $social_for_menu.='</ul>';

}


$args_menu=array(
    'menu_class' => 'menu',
    'echo' => true,
    'container' => 'nav',
    'id_container'=> 'nav',
    'fallback_cb' => 'wp_page_menu',
    'items_wrap' => '<ul class="links">%3$s</ul>'.$social_for_menu,
    'depth' => 0,
    'theme_location' => 'header-menu'
);
?>

<?php wp_nav_menu($args_menu);?>
</nav>

<!-- Nav -->
<!--<nav id="nav">-->
<!--    <ul class="links">-->
<!--        <li><a href="index.html">Home</a></li>-->
<!--        <ul class="links">-->
<!--        <li><a href="index.html">Home</a></li>-->
<!--        </ul>-->
<!--        <li><a href="generic.html">Generic</a></li>-->
<!--        <li><a href="elements.html">Elements</a></li>-->
<!--    </ul>-->
<!--</nav>-->

