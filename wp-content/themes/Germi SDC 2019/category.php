<?php get_header(); ?>


<?php if (have_posts()){ ?>
    <section id="main" class="wrapper">
        <div class="container">

            <header class="major special">
                <?php my_archive_title('<h1>', '</h1>')?>
            </header>

        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
            <?php while (have_posts()){ the_post();?>
                <div class="4u 12u$(small) box-archive">
                    <a href="<?php echo get_permalink();?>">
                        <span class="image fit">
                            <?php if(has_post_thumbnail(get_the_ID())){ $url_thumb=get_the_post_thumbnail_url(get_the_ID(),'archive');}else{ $url_thumb=get_bloginfo('template_url').'/assets/css/images/ph-germi.png';}?>
                            <img src="<?php echo $url_thumb;?>" alt=" immagine <?php echo get_the_title();?>" />
                        </span>
                        <h2><?php echo get_the_title();?></h2>
                        <p><?php echo get_the_excerpt();?></p>

                    </a>
                </div>
            <?php }?>
            </div>
        </div>
    </section>

<?php }?>
<?php get_footer(); ?>
