<?php get_header(); ?>

<?php if (have_posts()){ ?>
    <?php while (have_posts()){ the_post();?>

        <section id="main" class="wrapper">
            <div class="container">

                <header class="major special">
                    <h2><?php echo get_the_title();?></h2>
                    <?php if (has_excerpt()){?>
                        <p><?php echo get_the_excerpt();?></p>
                    <?php } ?>
                </header>

                <?php if (has_post_thumbnail()){?>
                    <img class="img-evi" src="<?php echo get_the_post_thumbnail_url('','evi')?>" alt="Immagine <?php echo get_the_title();?>" />
                <?php } ?>
                <?php the_content();?>
            </div>
        </section>
        <?php if (is_singular('gallery')){
            if (!empty($gallery=get_field('gallery'))){ ?>


                <section>
                    <div class="container">
                        <h3>Foto</h3>
                        <div class="box alt">
                            <div class="row 50% uniform">
                                <ul id="lightgallery" class="list-unstyled row">
                                <?php foreach ($gallery as $photo){?>
                                                                        <li class="col-xs-6 col-sm-3 col-md-3"
                                        data-responsive="<?php echo $photo['sizes']['ga375']; ?> 375,
                                                         <?php echo $photo['sizes']['ga480']; ?> 480,
                                                         <?php echo $photo['sizes']['large']; ?> 800"
                                        data-src="<?php echo $photo['sizes']['ga1600']; ?>" data-sub-html="<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>">
                                        <a href="">
                                            <img class="img-responsive" src="<?php echo $photo['sizes']['gallery']; ?>">
                                        </a>
                                    </li>

<!--                                    <div class="4u">-->
<!--                                        <span class="image fit">-->
<!--                                            <img src="--><?php //echo $photo['sizes']['gallery'];?><!--" alt="--><?php //echo $photo['alt'];?><!--" />-->
<!--                                        </span>-->
<!--                                    </div>-->
                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

    <?php }

        }?>
    <?php } ?>
<?php } ?>
<?php get_footer(); ?>
