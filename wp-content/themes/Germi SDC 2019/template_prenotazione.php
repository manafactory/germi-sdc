<?php /* Template Name: Prenotazione */ ?>
<?php get_header(); ?>
<?php
  $idevento = get_query_var('idevento');
  $esito = get_query_var('esito');
  
  if($idevento && $esito) {
?>
	<?php if (have_posts()){ ?>
        <?php while (have_posts()){ the_post();?>
    
            <section id="main" class="wrapper">
                <div class="container">
    
                    <header class="major special">
                        <h2><?php echo get_the_title();?></h2>
                        <p>Evento: <?php echo get_the_title($idevento); ?></p>
                    </header>
    
                    <?php 
                    if($esito=='ok') { 
                        echo get_field('esito_positivo');
                    } else {
                        echo get_field('esito_negativo');
                    }
                    
                    ?>
                    <p><a href="http://www.germildc.it">Vai alla home</a></p>
                </div>
            </section>
        <?php } ?>
    <?php } ?>

<?php } else if($idevento) { ?>

	<?php if (have_posts()){ ?>
        <?php while (have_posts()){ the_post();?>
    
            <section id="main" class="wrapper">
                <div class="container">
    
                    <header class="major special">
                        <h2><?php echo get_the_title();?></h2>
                        <p>Evento: <?php echo get_the_title($idevento); ?></p>
                    </header>
    
                    <?php 
					$max = intval(get_field('max_booking',$idevento));
					$num = intval(get_field('num_booking',$idevento));
					$prezzo = intval(get_field('prezzo',$idevento));
					$dif = $max-$num;
					
					$data=get_field('data_evento',$idevento);

					$date = new DateTime($data);
					//$date->sub(new DateInterval('P1D')); //subtract period of 1 day
					$date->sub(new DateInterval('PT3H')); //subtract period of 3 hours
					$now = new DateTime();
					
					if($dif>0 && $date>$now) {
						
						if($prezzo>0) {
							the_field('pagamento');
						} else {
							the_field('gratuito');
						}
						
					} else {
						$eventi=get_field('sezione_eventi','option');
						echo "<p>".$eventi['no_booking']."</p>";
					}
					
					?>
                    
                </div>
            </section>
        <?php } ?>
    <?php } ?>

<?php } else { ?>
	<script language="javascript">
	window.location.href = '<?php bloginfo('url'); ?>';
    </script>
<?php } ?>

<?php get_footer(); ?>
