<?php /* Template Name: Contatti */ get_header(); ?>
<?php get_header(); ?>

<?php if (have_posts()){ ?>
    <?php while (have_posts()){ the_post();?>

        <section id="main" class="wrapper">
            <div class="container">

                <header class="major special">
                    <h2><?php echo get_the_title();?></h2>
                    <?php if (has_excerpt()){?>
                        <p><?php echo get_the_excerpt();?></p>
                    <?php } ?>
                </header>

                <?php if (has_post_thumbnail()){?>
                    <img src="<?php echo get_the_post_thumbnail_url()?>" alt="Immagine <?php echo get_the_title();?>" />
                <?php } ?>
                <?php the_content();?>
            </div>
        </section>

        <section id="main">
            <div class="container">
                <?php if (!empty($codiceform=get_field('codice_form'))){ echo do_shortcode($codiceform);}?>
            </div>
        </section>
    <?php } ?>
<?php } ?>
<?php get_footer(); ?>
