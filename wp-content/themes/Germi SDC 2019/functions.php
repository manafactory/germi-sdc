<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_germi_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Menu Principale', 'html5blank'), // Main Navigation
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_germi_menu'); // Add HTML5 Blank Menu
//add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether



require_once('inc/acf.php');
require_once('inc/post_type_and_taxonomies_functions.php');






function dd($to_print){
    echo '<pre>';
    print_r($to_print);
    echo '</pre>';
}



add_theme_support( 'post-thumbnails' );
add_image_size('gallery', 300, 200);
add_image_size('gallery_home', 255, 170, true);
add_image_size('archive', 380, 255, true);
add_image_size('evi', 1180, 460, true);
add_image_size('ga375', 375, 9999999);
add_image_size('ga480', 480, 9999999);
add_image_size('ga1600', 1600, 9999999);





function my_archive_title( $before = '', $after = '' ) {
    $title = get_my_archive_title();

    if ( ! empty( $title ) ) {
        echo $before . $title . $after;
    }
}

/**
 * Retrieve the archive title based on the queried object.
 *
 * @since 4.1.0
 *
 * @return string Archive title.
 */
function get_my_archive_title() {
    if ( is_category() ) {
        /* translators: Category archive title. 1: Category name */
        $title = sprintf( __( '%s' ), single_cat_title( '', false ) );
    } elseif ( is_tag() ) {
        /* translators: Tag archive title. 1: Tag name */
        $title = sprintf( __( 'Tag: %s' ), single_tag_title( '', false ) );
    } elseif ( is_author() ) {
        /* translators: Author archive title. 1: Author name */
        $title = sprintf( __( 'Author: %s' ), '<span class="vcard">' . get_the_author() . '</span>' );
    } elseif ( is_year() ) {
        /* translators: Yearly archive title. 1: Year */
        $title = sprintf( __( 'Year: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
    } elseif ( is_month() ) {
        /* translators: Monthly archive title. 1: Month name and year */
        $title = sprintf( __( 'Month: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
    } elseif ( is_day() ) {
        /* translators: Daily archive title. 1: Date */
        $title = sprintf( __( 'Day: %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
    } elseif ( is_post_type_archive() ) {
        /* translators: Post type archive title. 1: Post type name */
        $title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
    } elseif ( is_tax() ) {
        $tax = get_taxonomy( get_queried_object()->taxonomy );
        /* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
        $title = sprintf( __( '%1$s: %2$s' ), $tax->labels->singular_name, single_term_title( '', false ) );
    } else {
        $title = __( 'Archives' );
    }

    /**
     * Filters the archive title.
     *
     * @since 4.1.0
     *
     * @param string $title Archive title to be displayed.
     */
    return apply_filters( 'get_the_archive_title', $title );
}




function germi_eventi($query){
    if ($query->is_main_query()===false){
        return $query;
    }elseif (get_query_var('post_type') != 'eventi') {

        return $query;
    }else{

        if (get_query_var('post_type') == 'eventi' && is_post_type_archive('eventi')){

            if (isset($_GET) && (!empty($_GET['start']) ||  !empty($_GET['end']))){
                if (isset($_GET) && !empty($_GET['start'])){

                    $start=$_GET['start'];
                    $start_=explode(' ', $start);
                    $start__=explode('/', $start_[0]);

                    $start_f=$start__[2].'-'.$start__[1].'-'.$start__[0];


                    $past= array(
                        'key'		=> 'data_evento',
                        'compare'	=> '>=',
                        'value'		=> $start_f.' 00:00:00',
                        'type' => 'DATETIME',
                        'inclusive' => true,

                    );
                }else{
                    $past= array(
                        'key'		=> 'data_evento',
                        'compare'	=> '>=',
                        'value'		=> date('Y-m-d').' 00:00:00',
                        'type' => 'DATETIME',
                        'inclusive' => true,

                    );
                }

                if (isset($_GET) && !empty($_GET['end'])){

                    $end=$_GET['end'];
                    $end_=explode(' ', $end);
                    $end__=explode('/', $end_[0]);

                    $end_f=$end__[2].'-'.$end__[1].'-'.$end__[0];

                    $future= array(
                        'key'		=> 'data_evento',
                        'compare'	=> '<=',
                        'value'		=> $end_f.' 00:00:00',
                        'type' => 'DATETIME',
                        'inclusive' => true,
                    );

                }else{
                    $future= array(
                        'key'		=> 'data_evento',
                        'compare'	=> '<=',
                        'value'		=> date('Y-m-d').' 00:00:00',
                        'type' => 'DATETIME',
                        'inclusive' => true,

                    );
                }

                $metaquery=array();




                if (!empty($past)){ array_push($metaquery, $past); }
                if (!empty($future)){ array_push($metaquery, $future); }

                if (!empty($metaquery)){
                    if (!empty($start) && !empty($end)){
                        $metaquery['relation'] = 'AND';
                    }

                    $query->set('meta_query', $metaquery);
                }
            }else{
                dd(date('Y-m-d'));

                $today= array(
                    'key'		=> 'data_evento',
                    'compare'	=> '>=',
                    'value'		=> date('Y-m-d').' 00:00:00',
                    'type' => 'DATETIME',
                    'inclusive' => true,

                );

                $metaquery=array();
                array_push($metaquery, $today);
                $query->set('meta_query', $metaquery);

            }


        }

        $query->set('meta_key', 'data_evento' );
        $query->set('orderby', array('meta_value' => 'ASC'));

        return $query;


    }

}

add_action('pre_get_posts', 'germi_eventi');


function quando($data){
    if (!empty($data)){

        $data_split=explode(' ', $data);

        if (isset($data_split[0]) && isset($data_split[1])){
            $data_dmy=explode('-',$data_split[0]);

            if(isset($data_dmy[0]) && isset($data_dmy[1]) && isset($data_dmy[2])){
                $data_to_show=$data_dmy[2].'/'.$data_dmy[1].'/'.$data_dmy[0];

            }

            $ora_split=explode(':', $data_split[1]);

            $data_to_show.=' - Ore '.$ora_split[0].':'.$ora_split[1];
        }


    }else{
        $data_to_show=$data;
    }
    return $data_to_show;
}


function my_pagination() {

    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){

        $current_page = max(1, get_query_var('paged'));


        echo paginate_links(array(
            'base' => add_query_arg( 'paged', '%#%' ),
            'format' => '',
            'show_all' => false,
            'end_size' => 1,
            'mid_size' => 2,
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text' => 'PREV',
            'next_text' => 'Next',
            'before_page_number' => '',
	'after_page_number'  => ''
        ));


    }

}


function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyBLERzGefFAPYEaBN6D5tGavewa119U9aA');
}

add_action('acf/init', 'my_acf_init');



add_filter( 'gform_field_value_requested-event', 'requested_event_function' );
function requested_event_function( $value ) {
	$idevento = get_query_var('idevento');
	$title = get_the_title( $idevento );
    return $title;
}

add_filter( 'gform_field_value_prezzo', 'event_price' );
function event_price( $value ) {
	$idevento = get_query_var('idevento');
	$price = get_field('prezzo',$idevento);
    return $price;
}

/*
add_filter( 'gform_field_value_id-event', 'id_event_function' );
function id_event_function( $value ) {
	$id = get_the_ID();
    return $id;
}
*/

add_action( 'gform_pre_submission_1', 'pre_submission_handler' );
add_action( 'gform_pre_submission_3', 'pre_submission_handler' );
function pre_submission_handler( $form ) {
	$id = rgpost( 'input_5' );
	$num_booking = get_post_meta($id,'num_booking',true);	
	$max_booking = get_post_meta($id,'max_booking',true);
	$disp = $max_booking - $num_booking;
	if($disp>0) {
    	$_POST['input_6'] = 'ok';
	} else {
    	$_POST['input_6'] = 'over booking';
	}
}

add_action( 'gform_after_submission_1', 'aggiorna_booking', 10, 2 );
function aggiorna_booking( $entry, $form ) {
	$id = rgar( $entry, '5' );
	$num_booking = get_post_meta($id,'num_booking',true);	
	$new_booking = $num_booking+1;	
	update_post_meta( $id, 'num_booking', $new_booking );
}


add_action( 'gform_paypal_post_ipn', 'update_order_status', 10, 4 );
function update_order_status( $ipn_post, $entry, $feed, $cancel ) {
 
    // if the IPN was canceled, don't process
    if ( $cancel )
        return;
 
	$id = rgar( $entry, '5' );
 	$num_booking = get_post_meta($id,'num_booking',true);	
	$new_booking = $num_booking+1;	
	update_post_meta( $id, 'num_booking', $new_booking );

}

function custom_query_vars_filter($vars) {
  $vars[] .= 'esito';
  $vars[] .= 'idevento';
  return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );


add_filter( 'gform_field_validation_2_10', function ( $result, $value, $form, $field ) {
    if ( $result['is_valid'] && $value == "No" ) {
        $result['is_valid'] = false;
        $result['message']  = 'Per proseguire occorre il consenso.';
    }
 
    return $result;
}, 10, 4 );

add_filter( 'gform_field_validation_2_11', function ( $result, $value, $form, $field ) {
    if ( $result['is_valid'] && $value == "No" ) {
        $result['is_valid'] = false;
        $result['message']  = 'Per proseguire occorre il consenso.';
    }
 
    return $result;
}, 10, 4 );

add_filter( 'gform_field_validation_2_12', function ( $result, $value, $form, $field ) {
    if ( $result['is_valid'] && $value == "No" ) {
        $result['is_valid'] = false;
        $result['message']  = 'Per proseguire occorre il consenso.';
    }
 
    return $result;
}, 10, 4 );


