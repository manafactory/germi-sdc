<?php get_header(); ?>

    <section id="main" class="wrapper">
        <div class="container">

            <header class="major special">
            <?php $eventi_evi=get_field('sezione_eventi','option'); ?>
			<h1><?php echo $eventi_evi['titolo_pagina_eventi'];?></h1>
            </header>

        </div>
    </section>
    <section class="eventi-filters">
        <div class="container">
            <h3>Filtra eventi</h3>
            <?php get_template_part("template-parts/content", "calendar");?>
        </div>
    </section>
<?php if (have_posts()){ ?>

    <section>
        <div class="container eventi">
            <div class="row">
                <?php while (have_posts()){ the_post(); ?>
                <div class="4u 12u$(small) box-archive">
                    <a href="<?php echo get_permalink();?>">
                        <span class="image fit">
                            <?php if(has_post_thumbnail(get_the_ID())){ $url_thumb=get_the_post_thumbnail_url(get_the_ID(),'archive');}else{ $url_thumb=get_bloginfo('template_url').'/assets/css/images/ph-germi.png';}?>
                            <img src="<?php echo $url_thumb;?>" alt=" immagine <?php echo get_the_title();?>" />
                        </span>
                        <h2><?php echo get_the_title();?></h2>
                        <?php $luogo=get_field('luogo_evento'); $quando=get_field('data_evento');?>

                        <span class="date"><?php echo quando($quando);?> <?php if (!empty($luogo) && isset($luogo['address']) && !empty($luogo['address'])){ echo ' - '.$luogo['address'];} ?> </span>

                        <p><?php echo get_the_excerpt();?></p>

                    </a>
                </div>
            <?php }?>
            </div>
        </div>
    </section>

    <section id="pagination">
        <div class="container">
            <div class="row">
                <div class="pagination">
                    <?php my_pagination(); ?>
                </div>
            </div>
        </div>
    </section>

<?php }else{?>
<section>
    <div class="container eventi">
        <div class="row">
            <h3>Nessun evento nell'intervallo selezionato.</h3>
        </div>
    </div>
</section>
<?php } ?>
<?php get_footer(); ?>
