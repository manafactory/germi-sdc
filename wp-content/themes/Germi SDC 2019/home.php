<?php
/**
 * Created by PhpStorm.
 * User: alessandroambrosini
 * Date: 04/02/19
 * Time: 12:49
 * Homepage template
 */

get_header();
?>

<?php
$apertura=get_field('apertura_home', 'option');
$overlay=get_bloginfo('template_url').'/assets/css/images/overlay.png';

$placeholder=get_bloginfo('template_url').'/assets/css/images/ph-germi.png';

$style_bg='';
if (!empty($apertura)){
    if (!empty($apertura['immagine'])){
        $immagine_url=$apertura['immagine']['url'];
        $immagine_url=$apertura['immagine']['url'];
        $style_bg='background-size: auto; background-position: top; min-height:400px';

    }
    $testo_principale=$apertura['testo_principale'];
    $testo_secondario=$apertura['testo_secondario'];
    $link=$apertura['link'];
    $cta=$apertura['call_to_action'];
}

if (empty($immagine_url)) {
    $immagine_url = get_bloginfo('template_url') . '/images/immagine_locale.jpg';
}
if (empty($testo_principale)) {
    $testo_principale = get_bloginfo('name');
}
if (empty($testo_secondario)) {
    $testo_secondario = get_bloginfo('description');
}
if (empty($cta)) {

    $cta='Leggi';
}

?>

<!-- Banner -->
<section id="banner" style="background-image: url('<?php echo $overlay;?>'), url('<?php echo $immagine_url;?>'); <?php echo $style_bg;?>">
<!--    <i class="icon fa-diamond" style="border: solid 2px #fff0; color: #60313100;"></i>-->

<h2><?php echo $testo_principale;?></h2>
<h3><?php echo $testo_secondario;?></h3>
    <?php if (!empty($link) ){?>
    <ul class="actions">
        <li><a href="<?php echo $link;?>" class="button big special"><?php echo $cta;?></a></li>
    </ul>
    <?php } ?>
</section>


<?php $eventi_evi=get_field('sezione_eventi','option'); ?>
<!-- One -->

<?php if (!empty($eventi_evi)){ $class_box_evento='left';?>
<section id="one" class="wrapper style1">
    <div class="inner">
        <header class="major narrow	special">
            <h2 class="eventi-evidenza"><?php echo $eventi_evi['titolo_sezione_eventi'];?></h2>
            <p><?php echo $eventi_evi['sottotitolo_sezione_eventi'];?></p>
        </header>
        <?php foreach ($eventi_evi['eventi_in_evidenza'] as $evento){ ?>
        <article class="feature <?php echo $class_box_evento;?>">
            <span class="image">
                <?php if (has_post_thumbnail($evento->ID)){ $url_thumb=get_the_post_thumbnail_url($evento->ID);}else{ $url_thumb=$placeholder;}?>
                <img src="<?php echo $url_thumb;?>" alt="immagine <?php echo get_the_title($evento->ID);?>" />
            </span>
            <div class="content eventi-evidenza">
                <h2><?php echo get_the_title($evento->ID);?></h2>
                <?php $luogo=get_field('luogo_evento', $evento->ID); ?>
                <span class="date"> <?php echo quando(get_field('data_evento', $evento->ID));?> <?php if (isset($luogo['address']) && !empty($luogo['address'])){echo ' - '.$luogo['address'];} ?></span>
                <p><?php get_the_excerpt($evento->ID);?></p>
                <ul class="actions">
                    <li>
                        <a href="<?php echo get_permalink($evento->ID);?>" class="button alt">More</a>
                    </li>
                </ul>
            </div>
        </article>
            <?php if ($class_box_evento=='left'){ $class_box_evento='right'; }else{ $class_box_evento='left'; }?>
        <?php } ?>
        <ul class="actions special">
            <li><a href="<?php echo get_post_type_archive_link('eventi');?>" class="button big alt">Tutti gli eventi</a></li>
        </ul>

    </div>
</section>
<?php } ?>


<?php $defaults = array(
    'numberposts' => 3,
    'category' => 1,
    'orderby' => 'date',
    'order' => 'DESC',
    'include' => array(),
    'exclude' => array(),
    'meta_key' => '',
    'meta_value' =>'', 'post_type' => 'post',
    'suppress_filters' => false
);
$news_hoem=get_posts($defaults);

?>

<?php if (!empty($news_hoem)){ ?>
<section id="one" class="wrapper style2 darked-red">
    <div class="inner">
        <header class="major narrow	special">
            <h2 class="eventi-evidenza">News</h2>
<!--            <p>--><?php //echo get_field('sottotitolo_sezione_eventi', 'option')?><!--</p>-->
        </header>
        <div class="row">
            <?php foreach ($news_hoem as $news){?>
                <div class="4u 12u$(small)">
                    <a href="<?php echo get_permalink($news->ID);?>">
                        <span class="image fit">
                        <?php if (has_post_thumbnail($news->ID)){ $url_thumb=get_the_post_thumbnail_url($news->ID, 'gallery');}else{ $url_thumb=$placeholder;}?>

                            <img src="<?php echo $url_thumb;?>" alt="Immagine <?php echo get_the_title($news->ID);?>" />
                        </span>
                        <h2><?php echo get_the_title($news->ID);?></h2>
                        <?php if (has_excerpt($news->ID)){?>
                        <p><?php echo get_the_excerpt($news->ID);?></p>
                        <?php } ?>

                    </a>
                </div>
            <?php } ?>
        </div>
        <ul class="actions special">
            <li><a href="<?php echo get_term_link(1);?>" class="button big alt">Tutte le news</a></li>
        </ul>

    </div>
</section>
<?php } ?>

<?php $defaults = array(
    'numberposts' => 4,
    'category' => '',
    'orderby' => 'date',
    'order' => 'DESC',
    'include' => array(),
    'exclude' => array(),
    'meta_key' => '',
    'meta_value' =>'', 'post_type' => 'gallery',
    'suppress_filters' => false
);
$gallery_hoem=get_posts($defaults);

?>

<?php if (!empty($gallery_hoem)){ ?>
<?php $intro_gallery=get_field('sezione_gallery','option'); ?>

<!-- Two -->
<section id="two" class="wrapper special">
    <div class="inner">
        <header class="major narrow">
            <h2><?php echo $intro_gallery['titolo_sezione_gallery'];?></h2>
            <p><?php echo $intro_gallery['sottotitolo_sezione_gallery'];?></p>
        </header>
        <div class="image-grid">
            <?php foreach ($gallery_hoem as $ga){ ?>
                <?php
                if (has_post_thumbnail($ga->ID)){
                    $img_gallery=get_the_post_thumbnail_url($ga->ID, 'gallery_home');
                    $img_gallery_alt='Immagine '.get_the_title($ga->ID);
                }else{
                    if (!empty($gallery_ga=get_field('gallery_home', $ga->ID))){
                        $img_gallery=$gallery_ga[0]['url'];
                        $img_gallery_alt=$gallery_ga[0]['alt'];
                    }
                }
                ?>
            <a href="<?php echo get_permalink($ga->ID);?>" class="image">
                <img src="<?php echo $img_gallery; ?>" alt="<?php echo $img_gallery_alt;?>" />
            </a>
            <?php } ?>
        </div>
        <ul class="actions">
            <li><a href="<?php echo get_post_type_archive_link('gallery');?>" class="button big alt">Tutte le gallery</a></li>
        </ul>
    </div>
</section>
<?php } ?>

<!-- Three -->
<!--<section id="three" class="wrapper style3 special">-->
<!--    <div class="inner">-->
<!--        <header class="major narrow	">-->
<!--            <h2>Magna sed consequat tempus</h2>-->
<!--            <p>Ipsum dolor tempus commodo turpis adipiscing Tempor placerat sed amet accumsan</p>-->
<!--        </header>-->
<!--        <ul class="actions">-->
<!--            <li><a href="#" class="button big alt">Magna feugiat</a></li>-->
<!--        </ul>-->
<!--    </div>-->
<!--</section>-->
<!---->
<!-- Four -->
<!--<section id="four" class="wrapper style2 special">-->
<!--    <div class="inner">-->
<!--        <header class="major narrow">-->
<!--            <h2>Get in touch</h2>-->
<!--            <p>Ipsum dolor tempus commodo adipiscing</p>-->
<!--        </header>-->
<!--        <form action="#" method="POST">-->
<!--            <div class="container 75%">-->
<!--                <div class="row uniform 50%">-->
<!--                    <div class="6u 12u$(xsmall)">-->
<!--                        <input name="name" placeholder="Name" type="text" />-->
<!--                    </div>-->
<!--                    <div class="6u$ 12u$(xsmall)">-->
<!--                        <input name="email" placeholder="Email" type="email" />-->
<!--                    </div>-->
<!--                    <div class="12u$">-->
<!--                        <textarea name="message" placeholder="Message" rows="4"></textarea>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <ul class="actions">-->
<!--                <li><input type="submit" class="special" value="Submit" /></li>-->
<!--                <li><input type="reset" class="alt" value="Reset" /></li>-->
<!--            </ul>-->
<!--        </form>-->
<!--    </div>-->
<!--</section>-->

<?php get_footer(); ?>
