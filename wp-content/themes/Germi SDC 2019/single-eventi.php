<?php get_header(); ?>

<?php if (have_posts()){ ?>
    <?php while (have_posts()){ the_post();?>
        <section id="main" class="wrapper">
            <div class="container">

                <header class="major special">
                    <h2><?php echo get_the_title();?></h2>
                    <?php if (has_excerpt()){?>
                        <p><?php echo get_the_excerpt();?></p>
                    <?php } ?>
                </header>
            </div>
        </section>

        <section>
            <div class="container">

                <div class="row">
                    <div class="6u 12u$(xsmall)">

                        <?php if (has_post_thumbnail()){?>
                            <img class="img-evento" src="<?php echo get_the_post_thumbnail_url()?>" alt="Immagine <?php echo get_the_title();?>" />
                        <?php } ?>

                    </div>

                    <div class="6u 12u$(xsmall)">
                        <h3>Dettagli evento</h3>
                        <?php $data=get_field('data_evento'); $luogo=get_field('luogo_evento'); ?>
                        <?php if (!empty($data) || !empty($luogo) || have_rows('altre_info')){ ?>
                            <ul class="alt">
                                <?php if (!empty($data)){ ?>

                                    <li>Quando: <?php echo quando($data);?></li>
                                <?php } ?>
                                <?php  if (!empty($luogo)){?>

                                    <li>Dove: <?php echo $luogo['address'];?></li>
                                <?php } ?>
                                <?php if (have_rows('altre_info')){ ?>
                                    <?php while (have_rows('altre_info')){ the_row();?>
                                        <li><?php echo get_sub_field('etichetta');?>: <?php echo get_sub_field('valore');?></li>
                                    <?php } ?>
                                <?php } ?>




				<?php 
					$eventi_evi=get_field('sezione_eventi','option'); 
					$tipo = get_field('prenotazione');
					$max = intval(get_field('max_booking'));
					$num = intval(get_field('num_booking'));
					$dif = $max-$num;
				?>
                
		<?php if($tipo=="link") {	?>
            <li class="booking"><span class="title"><?php echo $eventi_evi['titolo_booking_esterno'];?></span>
            <br /><?php echo $eventi_evi['intro_booking_esterno'];?></li>
            <li class="pulsanti"><a href="<?php echo get_field('link_prenotazione');?>" class="button big special" target="_blank">Ho gi&agrave; la tessera ACSI</a></li>
            <li class="pulsanti"><a href="<?php bloginfo('url');?>/tesseramento" class="button big alt">Non ho ancora la tessera</a></li>

        <?php } else { ?>
                
				<?php if($max>0) { ?>
                        <?php $prezzo = get_field('prezzo'); ?>
                        
						<?php if($prezzo>0) { ?>
	                        <li class="booking"><span class="title"><?php echo $eventi_evi['titolo_booking_pagamento'];?></span>
	                        <br /><?php echo $eventi_evi['intro_booking_pagamento'];?></li>
						<?php } else { ?>
	                        <li class="booking"><span class="title"><?php echo $eventi_evi['titolo_booking'];?></span>
	                        <br /><?php echo $eventi_evi['intro_booking'];?></li>
						<?php } 
                                                						
						
							$data=get_field('data_evento');
							$date = new DateTime($data);
							//$date->sub(new DateInterval('P1D')); //subtract period of 1 day
							$date->sub(new DateInterval('PT3H')); //subtract period of 3 hours
							$now = new DateTime();
						
							$test = $date->format('Y-m-d H:i:s');
							echo "<!--".$test."-->";
							
							if($dif>0 && $date>$now) { ?>
		
								<li class="pulsanti"><a href="<?php bloginfo('url');?>/prenotazione?idevento=<?php echo get_the_ID();?>" class="button big special">Ho gi&agrave; la tessera ACSI</a></li>
								<li class="pulsanti"><a href="<?php bloginfo('url');?>/tesseramento" class="button big alt">Non ho ancora la tessera</a></li>
		
							<?php } else { ?>
								<li><span class="alert"><?php echo $eventi_evi['no_booking'];?></span></li>
							<?php } // if dif & date ?>
                            
                        
                    <?php } //if max ?>

			<?php }  // if link ?>

                            </ul>
                        <?php }else{ ?>
                            <p>Nessun dettaglio.</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section id="content" class="content">
            <div class="container">
                <?php the_content();?>

            </div>
        </section>
    <?php } ?>
<?php } ?>
<?php get_footer(); ?>
