<?php
/**
 * Created by PhpStorm.
 * User: alessandroambrosini
 * Date: 13/02/19
 * Time: 11:13
 */




function register_custom_post_type_function()
{

    register_post_type('eventi', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Eventi', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Evento', /* Nome al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti gli Eventi', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Evento', /*  Testo per modifica */
            'new_item' => 'Nuovo Evento', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Evento', /* Testo per visualizzare */
            'search_items' => 'Cerca Evento', /* Testo per la ricerca*/
            'not_found' => 'Nessun Evento trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun Evento trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Evento', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-clock', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => array('slug' => 'evento'), /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */

    register_post_type('libri', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Libri', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Libro', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutti i libri', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica libro', /*  Testo per modifica */
            'new_item' => 'Nuovo libro', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza libro', /* Testo per visualizzare */
            'search_items' => 'Cerca libro', /* Testo per la ricerca*/
            'not_found' => 'Nessun libro trovato', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessun libro trovato nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Libro', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-book', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => array('slug' => 'libro'), /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */


    register_post_type('Gallery', /* nome del custom post type */
        // aggiungiamo ora tutte le impostazioni necessarie, in primis definiamo le varie etichette mostrate nei menù
        array('labels' => array(
            'name' => 'Gallery', /* Nome, al plurale, dell'etichetta del post type. */
            'singular_name' => 'Gallery', /* Nome, al singolare, dell'etichetta del post type. */
            'all_items' => 'Tutte le Gallery', /* Testo mostrato nei menu che indica tutti i contenuti del post type */
            'add_new' => 'Aggiungi nuovo', /* Il testo per il pulsante Aggiungi. */
            'add_new_item' => 'Aggiungi nuovo', /* Testo per il pulsante Aggiungi nuovo post type */
            'edit_item' => 'Modifica Gallery', /*  Testo per modifica */
            'new_item' => 'Nuova Gallery', /* Testo per nuovo oggetto */
            'view_item' => 'Visualizza Gallery', /* Testo per visualizzare */
            'search_items' => 'Cerca Gallery', /* Testo per la ricerca*/
            'not_found' => 'Nessuna Gallery trovata', /* Testo per risultato non trovato */
            'not_found_in_trash' => 'Nessuna Gallery trovata nel cestino', /* Testo per risultato non trovato nel cestino */
            'parent_item_colon' => ''
        ), /* Fine dell'array delle etichette */
            'description' => 'Gallery', /* Una breve descrizione del post type */
            'public' => true, /* Definisce se il post type sia visibile sia da front-end che da back-end */
            'publicly_queryable' => true, /* Definisce se possono essere fatte query da front-end */
            'exclude_from_search' => false, /* Definisce se questo post type è escluso dai risultati di ricerca */
            'show_ui' => true, /* Definisce se deve essere visualizzata l'interfaccia di default nel pannello di amministrazione */
            'query_var' => true,
            'menu_position' => '', /* Definisce l'ordine in cui comparire nel menù di amministrazione a sinistra */
            'menu_icon' => 'dashicons-camera', /* Scegli l'icona da usare nel menù per il posty type */
            'rewrite' => array('slug' => 'gallery'), /* Puoi specificare uno slug per gli URL */
            'has_archive' => true, /* Definisci se abilitare la generazione di un archivio (equivalente di archive-libri.php) */
            'capability_type' => 'post', /* Definisci se si comporterà come un post o come una pagina */
            'hierarchical' => false, /* Definisci se potranno essere definiti elementi padri di altri */
            /* la riga successiva definisce quali elementi verranno visualizzati nella schermata di creazione del post */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'revisions', 'sticky')
        ) /* fine delle opzioni */
    ); /* fine della registrazione */

}


// Uncategorized ID is always 1
wp_update_term(1, 'category', array(
    'name' => 'News',
    'slug' => 'News',
    'description' => ''
));

add_action( 'init', 'register_custom_post_type_function');