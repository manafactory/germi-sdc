(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		
	});
	
})(jQuery, this);




/* Toggle Datepicker */

var $ =jQuery.noConflict();


$(document).ready(function() {
    $('.toggle-datepicker').click(function(e) {
        e.preventDefault();
        console.log("3")
        $(this).toggleClass('active');
        $('.section_datepicker').slideToggle(200);
    });
});

/* End Toggle Datepicker */


/* Reveal */

$(document).ready(function() {
    $('.reveal-trigger').click(function(e) {
        e.preventDefault();
        console.log("4")
        $(this).parent('.reveal-content').toggleClass('active');
    });
});
