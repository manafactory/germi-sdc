<?php
/**
 * Created by PhpStorm.
 * User: matteobarale
 * Date: 31/05/18
 * Time: 12:24
 */


// [ginger_reset_cookie class="your-button-class" text="Reset Cookie"]
function ginger_reset_cookie_func( $atts ) {
    $a = shortcode_atts( array(
        'class' => '',
        'text' => 'Reset Cookie',
        'redirect_url' => ''
    ), $atts );
    $rest_cookie = '<button class="' .  $a["class"] . '" onclick="delete_cookie(\'ginger-cookie\', \'' . $a['redirect_url'].'\')">' . $a['text'] .'</button>';
    wp_enqueue_script('ginger-cookies-reset', plugin_dir_url(__FILE__) . "js/reset-cookie.js");

    return $rest_cookie;
}
add_shortcode( 'ginger_reset_cookie', 'ginger_reset_cookie_func' );