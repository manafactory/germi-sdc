<?php
/**
 * Created by PhpStorm.
 * User: matteobarale
 * Date: 06/06/18
 * Time: 11:03
 */



// create a scheduled event (if it does not exist already)
function ginger_share_data_activation() {

    $httpHost = str_replace('www.', '', $_SERVER['HTTP_HOST']);

    $rA = checkdnsrr($httpHost, "A");
    $rCname = checkdnsrr($httpHost, "CNAME");

    if (wp_next_scheduled('ginger_send_data_hook')){
        wp_clear_scheduled_hook('ginger_send_data_hook');
    }

    if (!wp_next_scheduled('ginger_share_data_hook') && ($rA || $rCname)) {
        wp_schedule_event(time(), 'ginger_weekly', 'ginger_share_data_hook');
    }
}
// and make sure it's called whenever WordPress loads
add_action('wp', 'ginger_share_data_activation');


function ginger_cron_schedules($schedules)
{
    if (!isset($schedules["ginger_weekly"])) {
        $schedules["ginger_weekly"] = array(
            'interval' => 60 * 60 * 24 * 7,
            'display'  => __('Ginger weekly'));
    }
    if (!isset($schedules["ginger_minutes"])) {
        $schedules["ginger_minutes"] = array(
            'interval' => 60,
            'display'  => __('Ginger minutes'));
    }
    return $schedules;
}

add_filter('cron_schedules', 'ginger_cron_schedules');


function ginger_send_data()
{

    $data_to_send = ginger_prepare_data();
    $data_to_send = json_encode($data_to_send);
    $x_auth = get_option('ginger_token');
    if(!$x_auth){
        $x_auth = ginger_create_site();
        if($x_auth === false) return;
    }

    $url = 'http://ginger.manafactory.it/site/update';
    $method = 'PUT';

    if ($x_auth) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_to_send);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_to_send),
            'x-auth: ' . $x_auth
        ));
        curl_exec($ch);
    }
}
add_action('ginger_share_data_hook', 'ginger_send_data');

function ginger_prepare_data()
{
    $data_to_send = [];
    // Site name
    $data_to_send['name'] = get_bloginfo('name');
    // Site url
    $data_to_send['url'] = get_bloginfo('url');
    // Wp Version
    $data_to_send['version'] = get_bloginfo('version');
    // Language
    $data_to_send['language'] = get_bloginfo('language');
    // Site Description
    $data_to_send['description'] = get_bloginfo('description', 'display');
    // Users number
    global $wp_roles;
    $roles = $wp_roles->roles;
    $count = 0;
    foreach ($roles as $key => $role) {
        $blog_users_query = ['role' => $key];
        if (is_multisite()) {
            $blog_users_query['blog_id'] = get_current_blog_id();
        }
        $blogusers = new WP_User_Query($blog_users_query);
        // Array of WP_User objects.
        $data_to_send['users_type'][$count]['name'] = $role['name'];
        $data_to_send['users_type'][$count]['count'] = $blogusers->get_total();
        $count++;
    }
    // Post type
    $post_types = get_post_types('', 'objects');
    $count = 0;
    foreach ($post_types as $key => $post_type) {
        if (
            $post_type->name == 'revision'
            || $post_type->name == 'nav_menu_item'
            || $post_type->name == 'custom_css'
            || $post_type->name == 'customize_changeset'
            || $post_type->name == 'oembed_cache'
            || $post_type->name == 'user_request'
        ) continue;

        $data_to_send['posts_type'][$count]['count'] = wp_count_posts($post_type->name)->publish;
        $data_to_send['posts_type'][$count]['label'] = $post_type->label;
        $data_to_send['posts_type'][$count]['name'] = $post_type->name;
        $count++;
    }
    // Taxonomies
    $count = 0;
    $taxonomies = get_taxonomies('', 'objects');
    foreach ($taxonomies as $taxonomy) {
        if (
            $taxonomy->name == 'post_tag'
            || $taxonomy->name == 'nav_menu'
            || $taxonomy->name == 'link_category'
            || $taxonomy->name == 'post_format'
            || $taxonomy->name == 'post_translations'
            || $taxonomy->name == 'term_language'
            || $taxonomy->name == 'term_translations'
        ) continue;

        $data_to_send['taxonomies'][$count]['count'] = wp_count_terms($taxonomy->name);
        $data_to_send['taxonomies'][$count]['label'] = $taxonomy->label;
        $data_to_send['taxonomies'][$count]['name'] = $taxonomy->name;

        $terms = get_terms($taxonomy->name, array(
            'hide_empty' => true,
        ));
        foreach ($terms as $term) {
            $data_to_send['taxonomies'][$count]['terms'][] = [
                'name'        => $term->name,
                'count'       => $term->count,
                'description' => $term->description
            ];
        }
        $count++;
    }

    // Plugin
    if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $all_plugins = get_plugins();
    if($all_plugins){
        foreach ($all_plugins as $plugin){
            $data_to_send['plugins'][] = $plugin;
        }
    }

    if($statisitc = get_option('ginger_statistic_share')){
        if($statisitc == '1'){
            $data_to_send['statistic'] = true;
        }
    }

    if($statisitc = get_option('ginger_informations_share')){
        if($statisitc == '1'){
            $data_to_send['information'] = true;
        }
    }

    return $data_to_send;

}



function ginger_create_site(){


    $data_to_send = json_encode(['url' => get_bloginfo('url')]);
    $url = 'http://ginger.manafactory.it/site/create';
    $method = 'POST';

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_to_send);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_to_send),
    ));
    $result = curl_exec($ch);
    list($headers, $content) = explode("\r\n\r\n",$result,2);
    foreach (explode("\r\n",$headers) as $hdr){
        $checkHeaderValue = explode(':', $hdr);
        if($checkHeaderValue[0] === 'x-auth'){
            update_option('ginger_site_created', 'true', false);
            update_option('ginger_token', trim($checkHeaderValue[1]), false);
            return trim($checkHeaderValue[1]);
        }
    }
    return false;
}