
<h1><?php _e("Need More? Try this tools!", "ginger"); ?></h1>

<h3><?php _e("Privacy Policy","ginger"); ?></h3>
<p><?php _e("You need help in writing your Privacy Policy?","ginger"); ?></p>

<a target="_blank" href="http://iubenda.refr.cc/VGW2J8C" class="button button-hero button-primary"><?php _e("Try <b>Iubenda</b>  with 10% discount","ginger"); ?></a>
<hr>

<h3><?php _e("GDPR","ginger"); ?></h3>

<p><?php _e("WordPress's built in export/erase tools only access the data on your site. What about the off-site user data?","ginger"); ?></p>
<p><?php _e("Privacywp is a simple one-stop-shop plugin you can easily provide and/or erase ALL of the end user data you need no matter where it's stored.","ginger"); ?></p>
<a target="_blank" href="https://privacywp.com/ref/4/?campaign=ginger" class="button button-hero button-primary"><?php _e("Get <b>Privacy WP</b> from here to be GDPR-compliant","ginger"); ?></a>