<?php
/**
 * Created by PhpStorm.
 * User: matteobarale
 * Date: 31/05/18
 * Time: 11:36
 */

?>

<h3>
    <?php _e("Ginger Shortcode", "ginger"); ?>
</h3>

<p>
    <?php _e("In questa pagina sono visulizzati tutti gli shortcode disponibili", "ginger"); ?>
</p>

<p>
    <strong>[ginger_reset_cookie class="your-button-class" text="Reset Cookie" redirect_url="Some url tu redirect"]</strong>
    <button class="btn" data-clipboard-text='[ginger_reset_cookie class="" text="" redirect_url=""]'>
        Copy to clipboard
    </button>
</p>