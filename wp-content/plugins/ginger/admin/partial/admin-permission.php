<?php
/**
 * Admin permission role page
 */

require_once  plugin_dir_path( __DIR__ ) . 'inc/admin-permission.php';
$registerdRoles = getAllRegisterdRole();
$selectedRole = getSelectedPermissionLevel();
?>


<h3>
    <?php _e("Admin Permission", "ginger"); ?>
</h3>

<p>
    <?php _e("Please, select minimum role can access to the settings page of Ginger", "ginger"); ?>
</p>

<div>
    <form>
        <?php foreach ($registerdRoles as $key => $role): ?>
            <?php $firstRole = get_first_role_permission($role['capabilities']); ?>
            <input
                    type="radio"
                    name="ginger_role_choice"
                    value="<?php echo $firstRole; ?>"
                    <?php checked( $selectedRole, $firstRole, true ); ?>
                    <?php if($firstRole == 'switch_themes' && $selectedRole == '') {
                        echo 'checked';
                    }?>
            > <?php echo $role['name']; ?>
            <br/>
        <?php endforeach ; ?>
    </form>
</div>




